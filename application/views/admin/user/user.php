<div class="container">
    <h3><?=$title?></h3>
    <hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
			<?php
				$id = isset($user) ? $user->id_user : 'null';
				echo form_open($link.'/'.$id);
			?>
				<div class="form-group">
					<label for="nama" class="control-label">Nama*</label>
					<?php
					$namauser = $this->user->namaUser();
					echo form_dropdown('nama', $namauser, isset($user) ? $user->nama : 'null', array(
						'class' => 'form-control',
						'id' => 'nama',
					));			
					?>
				</div>
				<div class="form-group">
					<label for="user" class="control-label">Username*</label>
					<?=form_input(array(
						'name' => 'user',
						'id' => 'user',
						'class' => 'form-control',
						'value' => isset($user) ? $user->username : null,
						'required' => 'required'
					)) ?>
				</div>
				<div class="form-group">
					<label for="user" class="control-label">Password*</label>
					<?=form_input(array(
							'name' => 'pass',
							'id' => 'pass',
							'class' => 'form-control',
							'value' => isset($user) ? $user->password : null,
							'required' => 'required'
						))
					?>
				</div>
				<div class="form-group">
					<label for="user" class="control-label">Hak Akses*</label>
					<?php
					$role = $this->user->roleUser();
					echo form_dropdown('id_role', $role, isset($user) ? $user->id_role : 'null', array(
						'class' => 'form-control'
					));			
					?>
				</div>
				<?=form_hidden('id', isset($user) ? $user->id_user : null)?>
				<div>
					<?=form_submit(array(
						'name' => $button,
						'value' => strtoupper($button),
						'class' => 'btn btn-primary'
					))?>
					<a href="<?=base_url('admin/data_user')?>" class="btn btn-danger">Data User</a>
				</div>
			<?php
			form_close()
			?>
        </div>
    </div>
</div>
<script type="text/javascript">
 $(document).ready(function() {
     $('#nama').select2({
      placeholder: 'Pilih Nama Pegawai',
      allowClear: false
     });
 });
</script>