<h3>Data User</h3>
<br>
<a href="<?=base_url('admin/add_user')?>" class="btn btn-info btn-sm">
    <span class="fa fa-lg fa-plus"></span>
    <span> Tambah User</span>
</a>
<br><br>
<div class="box box-success">
<div class="box-body">
    <table class="table table-bordered" id="dataUser">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Password</th>
                <th>Hak Akses</th>
				<th>Status</th>
				<th>Last Online</th>
				<th>Action</th>
            </tr>
          </thead>
          <tbody id="show_data">  
		  </tbody>
    </table>
</div>
</div>

<script>

	$(window).ready(function(){
		viewuser();
        $('#dataUser').DataTable();

		function viewuser(){
			$.ajax({
				type  : 'ajax',
				url   : '<?=base_url('admin/resUser')?>',
				async : false,
				dataType : 'json',
				success : function(data){
					if(data.success == false) {
						var html = '';
							html += "<tr>"+
									"<td class=\"text-center\" colspan=\"6\">Data Kosong</td>"+
									"</tr>";
						$('#show_data').html(html);
					} else {
						var html = '';
						var i;
						var j = 1;
						for(i=0; i<data.length; i++, j++){
							var confirm = "'Yakin ingin menghapus ?'";
							if(data[i].sts_online == '1'){
								var sts_online = '<td><span class="label label-success">Online</span></td>';
							} else {
								var sts_online = '<td><span class="label label-default">Offline</span></td>';
							}
							html += '<tr>'+
									'<td>'+j+'</td>'+
									'<td>'+data[i].nama+'</td>'+
									'<td>'+data[i].username+'</td>'+
									'<td>'+data[i].password+'</td>'+
									'<td>'+data[i].nama_role+'</td>'+
									sts_online+
									'<td>'+data[i].keterangan+'</td>'+
									'<td class="text-center">'+
									'<a href="'+'<?=base_url()?>'+'admin/edit_user/'+data[i].id_user+'" class="btn btn-warning fa fa-edit btn-sm"></a>'+
									'<a href="'+'<?=base_url()?>'+'user/del_user/'+data[i].id_user+'" class="btn btn-danger fa fa-trash btn-sm" onClick="return confirm('+confirm+')"></a>'+
									'</td>'+
									'</tr>';
						}$('#show_data').html(html);
					}
					
				},
				complete: function() {
					setTimeout(viewuser, 5000);
				}
			});
		};
		
    });
	
	
</script>
