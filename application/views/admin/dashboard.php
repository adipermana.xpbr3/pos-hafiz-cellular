<?php
  $uri = $this->uri->segment(2);
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=site_url('assets/adminlte/dist/img/user.png')?>" class="img-circle" alt="User Image">
        </div>
        <div class="info" style="margin-top:13px">
          <p><?=ucwords($this->session->nama," "); ?></p> 
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
		<?php

            $role_id = $this->session->userdata('id_role');
            $menu = $this->menu->getAllMenu($role_id)->result_array(); 

        ?>
		<!-- Looping Menu -->
		<?php foreach($menu as $m) : ?>
			<?php if($m['punya_anak']=='0') { ?>
				<li class="menu">
				  <a href="<?= base_url().$m['url_menu']?>">
					<i class="<?= $m['icon_menu']?>"></i> <span><?= $m['nama_menu']?></span>
				  </a>
				</li>
			<?php } else { ?>
					<li class="treeview">
						<a href="#">
							<i class="<?= $m['icon_menu']?>"></i>
							<span><?= $m['nama_menu']?></span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
					<!-- Sub Menu -->	
					<?php

						$menuId = $m['id_menu'];
						$querySubMenu = " SELECT *
											FROM `tb_sub_menu`
										   WHERE `id_menu` = $menuId
											 AND `is_active` = 1
										";

						$subMenu = $this->db->query($querySubMenu)->result_array();

					?>
					<?php foreach($subMenu as $sm) : ?>
							<li><a href="<?=base_url().$sm['url_sub_menu']?>"><i class="<?=$sm['icon']?>"></i> <?=$sm['nama_submenu']?></a></li>
					<?php endforeach; ?>
						</ul>
					</li>
			<?php } ?>
		<?php endforeach; ?>
			
        <li class="">
          <a href="<?=base_url('auth/logout');?>">
            <i class="fa fa-sign-out-alt"></i> <span>Keluar</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">