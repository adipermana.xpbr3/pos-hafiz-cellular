<h3>Menu Management</h3>
<br>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li><a href="<?= base_url('admin/menuManagement')?>" >Menu</a></li>
	  <li class="active"><a href="<?= base_url('admin/subMenu')?>" >Sub Menu</a></li>
	  <li><a href="#tab_3" data-toggle="tab">Hak Akses Menu</a></li>
	</ul>
	
	<div class="tab-content">
		<div class="tab-pane active">
			<br>
			<a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#addSubMenu">
				<span class="fa fa-lg fa-plus"></span>
				<span> Tambah Sub Menu</span>
			</a>
			<br><br>
			<table class="table table-bordered" id="dataMenu">
				<thead>
					<tr>
						<th>No</th>
						<th>Menu Induk</th>
						<th>Nama Sub Menu</th>
						<th>Url</th>
						<th>Sts Aktif</th>
						<th>Action</th>
					</tr>
				  </thead>
				  <tbody id="show_data">  
				  </tbody>
			</table>
		</div>
	</div>
</div>

<!--Add Modal-->
<div class="modal fade" id="addSubMenu" tabindex="-1" aria-labelledby="addMenuLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Tambah Sub Menu</h4>
	  </div>
	  <form method="POST" action="<?= base_url('admin/addSubMenu')?>">
		<div class="modal-body">
			<div class="form-group">
			  <label>Menu Induk</label>
			  <select class="form-control" name="id_menu">
				<option value="">-- Pilih Menu --</option>
				<?php foreach($data as $data) :?>
					<option value="<?php echo $data['id_menu']?>"><?php echo $data['nama_menu']?></option>
				<?php endforeach; ?>
			  </select>
			</div>
		  <div class="form-group">
			<label>Nama Sub Menu</label>
			<input name="nama_submenu" type="text" class="form-control" placeholder="Masukan nama submenu..." autocomplete="off">
		  </div>
		  <div class="form-group">
			<label>Url</label>
			<input name="url_submenu" type="text" class="form-control" placeholder="Masukan url submenu..." autocomplete="off">
		  </div>
		  <div class="form-group">
			<input name="is_active" type="checkbox" value="1" id="defaultCheck1" checked>
			<label class="form-check-label" for="defaultCheck1">
			  Status Aktif
			</label>
		  </div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
		  <button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	  </form>
	</div>
  </div>
</div>
<!--Update Modal-->
<div id="modalUpdate"></div>

<script>

	$(window).ready(function(){
		viewmenu();
        $('#dataMenu').DataTable();
		function viewmenu(){
		
			$.ajax({
				type  : 'ajax',
				url   : '<?=base_url('admin/dataSubMenu')?>',
				async : false,
				dataType : 'json',
				success : function(data){
					if(data.success == false) {
						var html = '';
							html += "<tr>"+
									"<td class=\"text-center\" colspan=\"6\">Data Kosong</td>"+
									"</tr>";
						$('#show_data').html(html);
					} else {
						var html = '';
						var modal = '';
						var i;
						var j = 1;
						var base_url = '<?=base_url('admin/updateSubMenu')?>';
						for(i=0; i<data.length; i++, j++){
							var confirm = "'Yakin ingin menghapus ?'";
							var id_sub_menu = data[i].id_sub_menu;
							if(data[i].is_active == '1'){
								var is_active = 'Aktif';
								var check = 'checked';
							} else {
								var is_active = 'Tidak Aktif';
								var check = '';
							}
							
							html += '<tr>'+
									'<td>'+j+'</td>'+
									'<td>'+data[i].nama_menu+'</td>'+
									'<td>'+data[i].nama_submenu+'</td>'+
									'<td>'+data[i].url_submenu+'</td>'+
									'<td>'+is_active+'</td>'+
									'<td class="text-center">'+
									'<a href="" id="btn_update" class="btn btn-warning fa fa-edit btn-sm" data-toggle="modal" data-target="#updateSubMenu'+i+'"></a>'+
									'<a href="'+'<?=base_url()?>'+'admin/delSubMenu/'+data[i].id_sub_menu+'" class="btn btn-danger fa fa-trash btn-sm" onClick="return confirm('+confirm+')"></a>'+
									'</td>'+
									'</tr>';
									
							modal += '<div class="modal fade" id="updateSubMenu'+i+'" tabindex="-1" aria-labelledby="addMenuLabel" aria-hidden="true">'+
									  '<div class="modal-dialog">'+
										'<div class="modal-content">'+
										  '<div class="modal-header">'+
											'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
											  '<span aria-hidden="true">&times;</span></button>'+
											'<h4 class="modal-title">Tambah Sub Menu</h4>'+
										  '</div>'+
										  '<form method="POST" action="'+base_url+'">'+
											'<div class="modal-body">'+
												'<div class="form-group">'+
												  '<label>Menu Induk</label>'+
												  '<select class="form-control" name="id_menu">'+
													'<option value="'+data[i].id_menu+'">'+data[i].nama_menu+'</option>'
													
											$.ajax({
													type  : 'ajax',
													url   : '<?=base_url('admin/dataMenu')?>',
													async : false,
													dataType : 'json',
													success : function(data){
															var z;
															for(z=0; z<data.length; z++){
																
																modal += '<option value="'+data[z].id_menu+'">'+data[z].nama_menu+'</option>'
															}
														}
													});						

													
							modal +=			  '</select>'+
												'</div>'+
											  '<div class="form-group">'+
												'<label>Nama Sub Menu</label>'+
												'<input name="id_sub_menu" value="'+id_sub_menu+'" type="hidden" class="form-control" placeholder="Masukan nama submenu..." autocomplete="off">'+
												'<input name="nama_submenu" value="'+data[i].nama_submenu+'" type="text" class="form-control" placeholder="Masukan nama submenu..." autocomplete="off">'+
											  '</div>'+
											  '<div class="form-group">'+
												'<label>Url</label>'+
												'<input name="url_submenu" value="'+data[i].url_submenu+'" type="text" class="form-control" placeholder="Masukan url submenu..." autocomplete="off">'+
											  '</div>'+
											  '<div class="form-group">'+
												'<input name="is_active" type="checkbox" value="1" id="defaultCheck1" '+check+'>'+
												'<label class="form-check-label" for="defaultCheck1">'+
												  'Status Aktif'+
												'</label>'+
											  '</div>'+
											'</div>'+
											'<div class="modal-footer">'+
											  '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>'+
											  '<button type="submit" class="btn btn-primary">Simpan</button>'+
											'</div>'+
										  '</form>'+
										'</div>'+
									  '</div>'+
									'</div>'
						};
						$('#show_data').html(html);
						$('#modalUpdate').html(modal);
					}
					
				}
			});
		};
		
    });		
	
</script>