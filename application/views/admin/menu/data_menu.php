<h3>Menu Management</h3>
<br>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li class="active"><a href="<?= base_url('admin/menuManagement')?>">Menu</a></li>
	  <li><a href="<?= base_url('admin/subMenu')?>">Sub Menu</a></li>
	  <li><a href="#tab_3">Hak Akses Menu</a></li>
	</ul>
	
	<div class="tab-content">
		<div class="tab-pane active">
			<br>
			<a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#addMenu">
				<span class="fa fa-lg fa-plus"></span>
				<span> Tambah Menu</span>
			</a>
			<br><br>
			<table class="table table-bordered" id="dataMenu">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Menu</th>
						<th>Icon</th>
						<th>Url</th>
						<th>Sts Aktif</th>
						<th>Action</th>
					</tr>
				  </thead>
				  <tbody id="show_data">  
				  </tbody>
			</table>
		</div>
	</div>
</div>

<!--Add Modal-->
<div class="modal fade" id="addMenu" tabindex="-1" aria-labelledby="addMenuLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Tambah Menu</h4>
	  </div>
	  <form method="POST" action="<?= base_url('admin/addMenu')?>">
		<div class="modal-body">
		  <div class="form-group">
			<label>Nama Menu</label>
			<input name="nama_menu" type="text" class="form-control" placeholder="Masukan nama menu..." autocomplete="off">
		  </div>
		  <div class="form-group">
			<label>Icon</label>
			<input name="icon" type="text" class="form-control" placeholder="Masukan nama icon..." autocomplete="off">
		  </div>
		  <div class="form-group">
			<label>Url</label>
			<input name="url" type="text" class="form-control" placeholder="Masukan url menu..." autocomplete="off">
		  </div>
		  <div class="form-group">
			<input name="is_active" type="checkbox" value="1" id="defaultCheck1" checked>
			<label class="form-check-label" for="defaultCheck1">
			  Status Aktif
			</label>
		  </div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
		  <button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	  </form>
	</div>
  </div>
</div>
<!--Update Modal-->
<div id="modalUpdate"></div>

<script>

	$(window).ready(function(){
		viewmenu();
        $('#dataMenu').DataTable();

		function viewmenu(){
			$.ajax({
				type  : 'ajax',
				url   : '<?=base_url('admin/dataMenu')?>',
				async : false,
				dataType : 'json',
				success : function(data){
					if(data.success == false) {
						var html = '';
							html += "<tr>"+
									"<td class=\"text-center\" colspan=\"6\">Data Kosong</td>"+
									"</tr>";
						$('#show_data').html(html);
					} else {
						var html = '';
						var modal = '';
						var i;
						var j = 1;
						var base_url = '<?=base_url('admin/updateMenu')?>';
						for(i=0; i<data.length; i++, j++){
							var confirm = "'Yakin ingin menghapus ?'";
							if(data[i].is_active == '1'){
								var is_active = 'Aktif';
								var check = 'checked';
							} else {
								var is_active = 'Tidak Aktif';
								var check = '';
							}
							html += '<tr>'+
									'<td>'+j+'</td>'+
									'<td>'+data[i].nama_menu+'</td>'+
									'<td>'+data[i].icon_menu+'</td>'+
									'<td>'+data[i].url_menu+'</td>'+
									'<td>'+is_active+'</td>'+
									'<td class="text-center">'+
									'<a href="" id="btn_update" class="btn btn-warning fa fa-edit btn-sm" data-toggle="modal" data-target="#updateMenu'+data[i].id_menu+'"></a>'+
									'<a href="'+'<?=base_url()?>'+'admin/delMenu/'+data[i].id_menu+'" class="btn btn-danger fa fa-trash btn-sm" onClick="return confirm('+confirm+')"></a>'+
									'</td>'+
									'</tr>';
									
							modal += '<div class="modal fade" id="updateMenu'+data[i].id_menu+'" tabindex="-1" aria-labelledby="updateMenuLabel" aria-hidden="true">'+
										'<div class="modal-dialog">'+
											'<div class="modal-content">'+
											  '<div class="modal-header">'+
												'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
												  '<span aria-hidden="true">&times;</span></button>'+
												'<h4 class="modal-title">Ubah Menu</h4>'+
											  '</div>'+
											  '<form method="POST" action="'+base_url+'">'+
												'<div class="modal-body">'+
												  '<div class="form-group">'+
													'<label>Nama Menu</label>'+
													'<input name="id_menu" type="hidden" class="form-control" placeholder="Masukan nama menu..." value ="'+data[i].id_menu+'" autocomplete="off">'+
													'<input name="nama_menu" type="text" class="form-control" placeholder="Masukan nama menu..." value ="'+data[i].nama_menu+'" autocomplete="off">'+
												  '</div>'+
												  '<div class="form-group">'+
													'<label>Icon</label>'+
													'<input name="icon" type="text" class="form-control" placeholder="Masukan nama icon..." value ="'+data[i].icon_menu+'" autocomplete="off">'+
												  '</div>'+
												  '<div class="form-group">'+
													'<label>Url</label>'+
													'<input name="url" type="text" class="form-control" placeholder="Masukan url menu..." value ="'+data[i].url_menu+'" autocomplete="off">'+
												  '</div>'+
												  '<div class="form-group">'+
													'<input name="is_active" type="checkbox" value="1" id="defaultCheck1" '+check+'>'+
													'<label class="form-check-label" for="defaultCheck1">'+
													' Status Aktif'+
													'</label>'+
												  '</div>'+
												'</div>'+
												'<div class="modal-footer">'+
												  '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>'+
												  '<button type="submit" class="btn btn-primary">Simpan</button>'+
												'</div>'+
											  '</form>'+
											'</div>'+
										  '</div>'+
									 '</div>'
						};
						$('#show_data').html(html);
						$('#modalUpdate').html(modal);
					}
					
				}
			});
		};
		
    });		
	
</script>