<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login - Sistem Point Of Sales Konter</title>
    <link rel="stylesheet" href="<?=base_url('assets/adminlte/bower_components/bootstrap4/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/fontawesome/css/fontawesome-all.min.css')?>">
    <link rel="icon" href="<?=base_url('img/logo/giftpos.png')?>" type="image/png">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <script src="<?=base_url('assets/js/jquery/jquery.min.js')?>"></script>
    <script src="<?=base_url('assets/sweetalert/sweetalert.min.js')?>"></script>
</head>
<body style="background-image:url(<?=base_url('img/bgl.png')?>)" >

<div class="d-flex justify-content-center py-4">
    <!--<span class="mx-2">
        <img src="<?=base_url('img/logo/giftpos.png')?>" alt="giftpost" width="40px">
    </span>
    <h3 class="text-white font-weight-bold mr-2">Hafiz</h3>
    <h3 class="text-white font-weight-light">Cellular</h3>-->
</div>

    <div class="my-5 py-5">
    
    </div>

    <?= form_open(site_url('auth/proses_login')); ?>
        <div class='d-flex justify-content-center'>
			<div class="py-4 px-4 col-lg-3">
				<div class="card shadow">
					<h3 class="text-center">Silakan Login</h3>
					<div class="card-body">
						<div class="form-group">    
							<?= form_label('Username');?>
							<?= form_input(array(
								'name'=>'user',
								'required'=>'required',
								'class'=>'form-control',
								'autofocus'=>'autofocus',
								'autocomplete'=>'off',
								'placeholder'=>'Masukan Username',
								'maxlength' => '20'
							));?>
						</div>
						<div class="form-group">
							<?= form_label('Password');?>
							<?= form_password(array(
								'name'=>'pass',
								'required'=>'required',
								'class'=>'form-control',
								'placeholder'=>'Masukan Password',
								'maxlength' => '20'
							));?>
						</div>
							<?= form_submit(array(
								'name'=>'login',    
								'value'=>'Login',
								'class'=>'btn btn-info btn-block my-4',
								'maxlength' => '20'
							));?>
						
						<p class="mt-3 text-muted text-center">This website is protected <i class="bi bi-shield-fill-check" style="color: #0EC7CA;"></i></p>
                        <p class="mt-1 mb-1 text-muted text-center">Copyright &copy; Hafiz Cellular <?php echo date('Y')?></p>
					</div>
				</div>
			</div>
        </div>
    <?=form_close();?>
</body>
<script src="<?= base_url('assets/adminlte/bower_components/bootstrap4/js/bootstrap.min.js')?>"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<?php if ($this->session->flashdata('failed')) {?>   
  <script type="text/javascript">
    $(function(){
        swal("Password Salah", "Coba Cek kembali password yang anda masukan !", "error");
    });
  </script>
<?php } else if ($this->session->flashdata('not found')) { ?>  
  <script type="text/javascript">
    $(function(){
        swal("Username Tidak Ditemukan", "Coba cek kembali username atau hubungi Administrator !", "error");
    });
  </script>
<?php }?>

</html>