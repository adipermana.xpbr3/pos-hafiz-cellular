<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if($this->session->userdata('id_role')!='99')
        {
            redirect('auth');
        }
        $this->load->model('m_pegawai', 'pegawai');

        $config['upload_path'] = './img/foto_pegawai/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 10048;
        $this->load->library('upload', $config);
    }

    public function index()
    {
        if ($this->session->level)
        {
            redirect('admin');
        }
    }

    public function add_pegawai ()
    {
        if ($_POST['tambah'])
        {
            /* if($this->upload->do_upload('foto'))
            { */	
				$this->upload->do_upload('foto');
				$max = $this->pegawai->maxPegawai()->row_array();
				$max = $max['id_pegawai'];
				$generate = (int)$max + 1;
				$id_pegawai = sprintf("%03s",$generate);
                $data = $this->input->post();
                $foto = $this->upload->data('orig_name');
				if (trim($foto) == '') {
					$foto = 'default.jpg';
				}
                $add = $this->pegawai->addPegawai($data, $foto, $id_pegawai);
                if ($add)
                {
                    $this->session->set_flashdata('success','Tambah Data Pegawai Berhasil');
                    redirect('admin/data_pegawai');
                }
                else
                {
                    $this->session->set_flashdata('failed','Tambah Data Pegawai Gagal');
                    redirect('admin/add_pegawai');
                }
            /* }
            else
            {
                $this->session->set_flashdata('failed','Tambah Data Pegawai Gagal');
                redirect('admin/add_pegawai');
            } */
        }
    }

    public function del_pegawai ($id)
    {	
		$id_user = $this->pegawai->getIdUser($id)->row_array();
		$id_user = $id_user['id_user'];
        $del = $this->pegawai->delPegawai($id);
        if ($del)
        {
			$this->pegawai->delUser($id_user);
            $this->session->set_flashdata('success', 'Hapus Data Berhasil');
            redirect('admin/data_pegawai');
        }
    }

    public function update_pegawai ($id)
    {
        //if ($this->upload->do_upload('foto'))
        //{
            $data = $this->input->post();
            $foto = $this->upload->data('orig_name');
			if (trim($foto) == '') {
				$foto = 'default.jpg';
			}
            $update = $this->pegawai->updatePegawai($data, $foto);
            
            if ($update)
            {
                $this->session->set_flashdata('success', 'Update Data Pegawai Berhasil');
                redirect('admin/data_pegawai');
            }
            else
            {
                $this->session->set_flashdata('failed', 'Update Data Pegawai Gagal');
                redirect('admin/edit_pegawai'.$id);
            }
        //}

    }
}