<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if($this->session->userdata('id_role')!='99')
        {
            redirect('auth');
        }
        $this->load->model('m_user', 'user');
    }

    public function index()
    {
        if ($this->session->level)
        {
            redirect('admin');
        }
    }

    public function add_user ()
    {
        $uname = $this->input->post('user');
        if ($this->user->getUsername($uname)->num_rows() > 0)
        {
            $this->session->set_flashdata('avaible', 'Username Sudah Ada');
            redirect('admin/add_user');
        }
        else
        {
			$max = $this->user->maxUser()->row_array();
			$max = $max['id_user'];
			$generate = (int)$max + 1;
			$id_user = sprintf("%03s",$generate);
			$nama = trim($this->input->post('nama'));
			$id_pegawai = $this->user->idPegawai($nama)->row_array();
			$id_pegawai = $id_pegawai['id_pegawai'];
            $data = $this->input->post();
            $add = $this->user->addUser($data, $id_user, $id_pegawai);		

            if ($add)
            {	
				$this->user->updateIdUser($id_user, $id_pegawai);	
                $this->session->set_flashdata('success', 'Tambah Data User Berhasil');
                redirect('admin/data_user');
            }
        }
    }

    public function del_user ($id)
    {
        $del = $this->user->delUser($id);
        if ($del)
        {
            $this->session->set_flashdata('success', 'Delete Data Berhasil');
            redirect('admin/data_user');
        }
    }

    public function update_user ()
    {
        $data = $this->input->post();
        $update = $this->user->updateUser($data);
        $uname = $this->input->post('username');

        if ($update)
        {
            $this->session->set_flashdata('success', 'Update Data Berhasil');
            redirect('admin/data_user');
        }
        else
        {
            $this->session->set_flashdata('failed', 'Update Data Gagal');
            redirect('admin/edit_user/'.$this->input->post('id'));
        }
    }
}