<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_user','user');

	}
	
	public function index()
	{
		if($this->session->has_userdata('id_role')){
			if ($this->session->id_role == '99'){
				redirect('admin');
			}elseif($this->session->id_role == '01'){
				redirect('kasir');
			}
		}else{
			$this->load->view('login');
		}
	}

    public function proses_login(){
        if( $this->input->post('login')){
			$data = $this->input->post();
			$cek = $this->user->ceklogin($data);
			$username = trim($this->input->post('user'));
			$password = trim($this->input->post('pass'));
			
			if ($cek->num_rows() > 0 )
			{	
				if($cek->row('password') == $password){
					if ($cek->row('id_role')=='99')
					{
						$sess_data = array(
							'id_user' => $cek->row('id_user'),
							'nama' => $cek->row('nama'),
							'username' => $cek->row('username'),
							'level' => $cek->row('level'),
							'id_role' => $cek->row('id_role')
						);
						$this->session->set_userdata($sess_data);
						$id_user = $cek->row('id_user');
						$this->user->updateLogin($id_user);
						redirect('admin');
					}
					elseif ($cek->row('id_role')=='01')
					{
						$sess_data = array(
							'id_user' => $cek->row('id_user'),
							'nama' => $cek->row('nama'),
							'username' => $cek->row('username'),
							'level' => $cek->row('level'),
							'id_role' => $cek->row('id_role')
						);
						$this->session->set_userdata($sess_data);
						$id_user = $cek->row('id_user');
						$this->user->updateLogin($id_user);
						redirect('kasir');
					}
				} else {
				
					$this->session->set_flashdata('failed', 'failed');
					redirect('auth');
				}
			}
			else
			{
				$this->session->set_flashdata('not found', 'not found');
				redirect('auth');
				
			}
        }
	}
	public function logout()
	{
		$id_user = $this->session->userdata('id_user');
		$this->user->updateLogout($id_user);
		$this->session->sess_destroy();
		redirect('auth');
	}
}
