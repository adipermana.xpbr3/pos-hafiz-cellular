<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	
	public function __construct()
	{
        parent::__construct();
        if($this->session->userdata('id_role')!='99')
        {
            redirect('auth');
        }
        $this->load->model('m_menu', 'menu');
    }
	
	public function add_menu ($data)
    {
		
		$send = $this->menu->addMenu($data);
		if($send){
			header('Content-Type: application/json');
			echo json_encode(
				array(
					'success' => true,
					'message' => 'data inserted', 
				)
			,JSON_PRETTY_PRINT);
		} else {
			header('Content-Type: application/json');
			echo json_encode(
				array(
					'success' => false,
					'message' => 'failed', 
				)
			,JSON_PRETTY_PRINT);
		}
	}
}