<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_laporan_a extends CI_Model {
    
    public function lap_bulan()
    {
        $this->db->select("MONTH(tanggal_beli) as bulan, YEAR(tanggal_beli) as tahun, SUM(qty) as terjual, SUM(subtotal) as total, SUM(subtotal - (harga_awal * qty)) as keuntungan");
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->group_by('MONTH(tanggal_beli)');
        $this->db->order_by('tanggal_beli', 'DESC');
        return $this->db->get();
    }

    public function lap_bulanan_detail($bulan, $tahun)
    {
        return $this->db->query("SELECT nama_b, tb_detailpembelian.harga_awal, tb_detailpembelian.harga_jual, sum(qty) as jumlah, SUM(subtotal) as total, SUM(subtotal - (tb_detailpembelian.harga_awal * qty)) as keuntungan from tb_pembelian JOIN tb_detailpembelian USING(id_pembelian) JOIN tb_barang USING(id_barang) where MONTH(tanggal_beli) = '$bulan' AND year(tanggal_beli)='$tahun' GROUP BY nama_b");
    }

    public function countTransaksi()
    {
        date_default_timezone_set('Asia/Jakarta');
        $bulan = date('m', time());
        $tahun = date('Y', time());

        $this->db->select('id_pembelian');
        $this->db->from('tb_pembelian');
        $this->db->where('MONTH(tanggal_beli)', $bulan);
        $this->db->where('YEAR(tanggal_beli)', $tahun);
        return $this->db->count_all_results();
    }

    public function countBrgTerjual()
    {
        date_default_timezone_set('Asia/Jakarta');
        $bulan = date('m', time());
        $tahun = date('Y', time());
        
        $this->db->select_sum('qty');
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->where('MONTH(tanggal_beli)', $bulan);
        $this->db->where('YEAR(tanggal_beli)', $tahun);
        return $this->db->get()->row();
    }
    
    public function labaRugiBulan()
    {
        date_default_timezone_set('Asia/Jakarta');
        $bulan = date('m', time());
        $tahun = date('Y', time());
        
        $this->db->select("SUM(subtotal - (harga_awal*qty)) as keuntungan");
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->where('MONTH(tanggal_beli)', $bulan);
        $this->db->where('YEAR(tanggal_beli)', $tahun);
        return $this->db->get()->row();
    }
	
	public function penjualanBulan()
    {
        date_default_timezone_set('Asia/Jakarta');
        $bulan = date('m', time());
        $tahun = date('Y', time());
        
        $this->db->select("SUM(subtotal) as penjualan");
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->where('MONTH(tanggal_beli)', $bulan);
        $this->db->where('YEAR(tanggal_beli)', $tahun);
        return $this->db->get()->row();
    }
    
    public function countTransaksiTgl($tgl)
    {
        $this->db->select("id_pembelian");
        $this->db->from("tb_pembelian");
        $this->db->where("DATE(tanggal_beli)", $tgl);
        return $this->db->count_all_results();
    }
    
    public function countBrgTerjualTgl($tgl)
    {
        $this->db->select("SUM(qty) as jumlah");
        $this->db->from("tb_pembelian");
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->where("DATE(tanggal_beli)", $tgl);
        return $this->db->get();
    }

    public function labaRugiTgl($tgl)
    {
        $this->db->select("SUM(subtotal - (harga_awal * qty)) as keuntungan");
        $this->db->from("tb_pembelian");
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->where("DATE(tanggal_beli)", $tgl);
        return $this->db->get();
    }
	
	public function penjualanTgl($tgl)
    {
        $this->db->select("SUM(subtotal) as penjualan");
        $this->db->from("tb_pembelian");
        $this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
        $this->db->where("DATE(tanggal_beli)", $tgl);
        return $this->db->get();
    }
	
	public function namaKasir($id_user)
	{
		if($id_user != null) {
			$this->db->select("id_user");
			$this->db->select("nama");
			$this->db->from("tb_user");
			$this->db->where("tb_user.id_user", $id_user);
			$this->db->order_by("id_user", "asc");
			$data = $this->db->get();
			
		} else {
			$this->db->select("id_user");
			$this->db->select("nama");
			$this->db->from("tb_user");
			$this->db->where("tb_user.id_role", "01");
			$this->db->order_by("id_user", "asc");
			$data = $this->db->get();
			
		}
		return $data;
	}
	
	public function penjualanKasir($id_user, $bulan, $tahun)
    {	
		//var_dump($id_user, $bulan, $tahun); die;
        if(($id_user != null) && ($bulan != null)){
			$message = 'id_user dan bulan';
			$this->db->select("tb_pembelian.id_user as id_user");
			$this->db->select("tb_pembelian.tanggal_beli as tgl_trans");
			$this->db->select("SUM(subtotal) as penjualan");
			$this->db->select("SUM(qty) as jumlah");
			$this->db->from('tb_pembelian');
			$this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
			$this->db->where('MONTH(tanggal_beli)', $bulan);
			$this->db->where('YEAR(tanggal_beli)', $tahun);
			$this->db->where('tb_pembelian.id_user', $id_user);
			$this->db->group_by('tb_pembelian.id_user');
			$this->db->order_by("DATE_FORMAT(tb_pembelian.tanggal_beli,'%Y%m')");
			$this->db->order_by('tb_pembelian.id_user', 'ASC');
			$data = $this->db->get();
			
		} else if($bulan == null && $id_user != null){
			$message = 'id_user';
			$this->db->select("tb_pembelian.id_user as id_user");
			$this->db->select("tb_pembelian.tanggal_beli as tgl_trans");
			$this->db->select("SUM(subtotal) as penjualan");
			$this->db->select("SUM(qty) as jumlah");
			$this->db->from('tb_pembelian');
			$this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
			$this->db->where('YEAR(tanggal_beli)', $tahun);
			$this->db->where('tb_pembelian.id_user', $id_user);
			$this->db->group_by('tb_pembelian.id_user');
			$this->db->order_by("DATE_FORMAT(tb_pembelian.tanggal_beli,'%Y%m')");
			$this->db->order_by('tb_pembelian.id_user', 'ASC');
			$data = $this->db->get();
			
		} else if($bulan != null && $id_user == null){
			$message = 'bulan';				
			$this->db->select("tb_pembelian.id_user as id_user");
			$this->db->select("tb_pembelian.tanggal_beli as tgl_trans");
			$this->db->select("SUM(subtotal) as penjualan");
			$this->db->select("SUM(qty) as jumlah");
			$this->db->from('tb_pembelian');
			$this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
			$this->db->where('year(tanggal_beli) =', $tahun);
			$this->db->where('month(tanggal_beli) =', $bulan);
			$this->db->group_by('tb_pembelian.id_user');
			$this->db->order_by("DATE_FORMAT(tb_pembelian.tanggal_beli,'%Y%m')");
			$this->db->order_by('tb_pembelian.id_user', 'ASC');
			$data = $this->db->get();
			
		} else {
			$message = 'tahun';	
			$this->db->select("tb_pembelian.id_user as id_user");
			$this->db->select("tb_pembelian.tanggal_beli as tgl_trans");
			$this->db->select("SUM(subtotal) as penjualan");
			$this->db->select("SUM(qty) as jumlah");
			$this->db->from('tb_pembelian');
			$this->db->join('tb_detailpembelian', 'tb_pembelian.id_pembelian = tb_detailpembelian.id_pembelian');
			$this->db->where('YEAR(tanggal_beli)', $tahun);
			$this->db->group_by('tb_pembelian.id_user');
			$this->db->order_by("DATE_FORMAT(tb_pembelian.tanggal_beli,'%Y%m')");
			$this->db->order_by('tb_pembelian.id_user', 'ASC');
			$data = $this->db->get();
		}

		return $data;
    }
}
