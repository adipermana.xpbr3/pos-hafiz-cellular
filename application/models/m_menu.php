<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_menu extends CI_Model {
	
	public function getAllMenu($role_id) {
		$queryMenu = "SELECT `tb_menu`.`id_menu`, `nama_menu`, `punya_anak`,`url_menu`, `icon_menu`
						FROM `tb_menu` JOIN `tb_access_menu` 
						  ON `tb_menu`.`id_menu` = `tb_access_menu`.`menu_id`
					   WHERE `tb_access_menu`.`role_id` = $role_id
						 AND `tb_menu`.`is_active` = 1
					ORDER BY `tb_menu`.`menu_order`,`tb_access_menu`.`menu_id` ASC 
					 ";
					 
		return $this->db->query($queryMenu);
	}
	
	public function maxMenu()
	{
		$this->db->select_max('id_menu');
		$this->db->from('tb_menu');
		$data = $this->db->get();
		return $data;
	}
	
	public function getMenu()
	{
		$this->db->select('*');
		$this->db->from('tb_menu');
		$data = $this->db->get();
		return $data;
	}
	
	public function updateMenu($id_menu, $data)
	{
		$this->db->where('id_menu', $id_menu);
        return $this->db->update('tb_menu', $data);
	}
	
		
	public function addMenu($data)
	{
        return $this->db->insert('tb_menu', $data);
	}	
	
	public function delMenu($id_menu)
	{
        $this->db->where('id_menu', $id_menu);
        return $this->db->delete('tb_menu');
	}
	
	public function getSubMenu()
	{
		$this->db->select('*');
		$this->db->from('tb_sub_menu');
		$data = $this->db->get();
		return $data;
	}
	
	public function getNamaMenu($id_menu)
	{
		$this->db->select('nama_menu');
		$this->db->from('tb_menu');
		$this->db->where('id_menu', $id_menu);
		$data = $this->db->get();
		return $data;
	}
	
	public function getNamaMenuAll()
	{
		$this->db->select('id_menu, nama_menu');
		$this->db->from('tb_menu');
		$data = $this->db->get();
		return $data;
	}
	
	public function maxSubMenu()
	{
		$this->db->select_max('id_sub_menu');
		$this->db->from('tb_sub_menu');
		$data = $this->db->get();
		return $data;
	}
	
	public function addSubMenu($data)
	{
        return $this->db->insert('tb_sub_menu', $data);
	}
	
	public function updateAnak($id_menu, $anak)
	{
		$this->db->where('id_menu', $id_menu);
        return $this->db->update('tb_menu', $anak);
	}
	
	public function updateSubMenu($id_submenu, $data)
	{
		$this->db->where('id_sub_menu', $id_submenu);
        return $this->db->update('tb_sub_menu', $data);
	}
	
	public function delSubMenu($id_sub_menu)
	{
        $this->db->where('id_sub_menu', $id_sub_menu);
		return $this->db->delete('tb_sub_menu');
	}
	
}