<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_user extends CI_Model {

    public function ceklogin ($data)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('username', $data['user']);
        //$this->db->where('password', $data['pass']);
        return $this->db->get();
    }

    public function addUser ($data, $id_user, $id_pegawai)
    {
        $param = array(
			'id_user' => $id_user,
            'username' => trim($data['user']),
            'password' => trim($data['pass']),
            'nama' => trim($data['nama']),
			'id_role' => trim($data['id_role']),
			'id_pegawai' => $id_pegawai,
        );
        return $this->db->insert('tb_user', $param);
    }

    public function getAllUser ($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
		$this->db->where('id_user !=', $id);
		$this->db->order_by('id_role','desc');
        return $this->db->get();
    }
	

    public function getUsername ($uname)
    {
        $this->db->select('username');
        $this->db->from('tb_user');
        $this->db->where('username', $uname);
        return $this->db->get();        
    }

    public function getDataUser ($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user', $id);
        $this->db->limit('1');
        return $this->db->get();
    }

    public function delUser($id)
    {
        $this->db->where('id_user', $id);
        return $this->db->delete('tb_user');
    }

    public function updateUser($data)
    {
        $param = array(
            'username' => trim($data['user']),
            'password' => trim($data['pass']),
            'nama' => trim($data['nama']),
            'level' => trim($data['level']),
			'id_role' => trim($data['id_role']),
        );
        $this->db->where('id_user', $data['id']);
        return $this->db->update('tb_user', $param);
    }
	
	public function maxUser()
	{
		$this->db->select_max('id_user');
		$this->db->from('tb_user');
		$this->db->where('id_user !=', '999');
		$data = $this->db->get();
		return $data;
	}
	
	public function roleUser()
	{
		$this->db->select('*');
		$this->db->from('tb_role');
		$this->db->order_by("nama_role", "asc");
		$data = $this->db->get();
		//var_dump($data->result_array()); die;
		foreach($data->result_array() as $row ){
        //this sets the key to equal the value so that
        //the pulldown array lists the same for each
        $array[$row['id_role']] = $row['nama_role'];
		}
		return $array;
	}
	
	public function namaUser()
	{
		$this->db->select('nama_p');
		$this->db->from('tb_pegawai');
		$this->db->order_by("nama_p", "asc");
		$data = $this->db->get();
		//$array = array("00"=>"-- Pilih Nama Pegawai --");
		foreach($data->result_array() as $row ){
        //this sets the key to equal the value so that
        //the pulldown array lists the same for each
        $array[$row['nama_p']] = $row['nama_p'];
		//array_push($array,$row['nama_p'],$row['nama_p']);
		}
		return $array;
	}
	
	public function idPegawai($nama)
	{
		$this->db->select('id_pegawai');
		$this->db->from('tb_pegawai');
		$this->db->where('nama_p =', $nama);
		$data = $this->db->get();
		return $data;
	}
	
	public function hakAkses($id_role)
	{
		$this->db->select('nama_role');
		$this->db->from('tb_role');
		$this->db->where('id_role =', $id_role);
		$data = $this->db->get();
		return $data;
	}
	
	public function updateIdUser ($id_user, $id_pegawai)
	{	
		$param = array(
            'id_user' => $id_user,
        );
        $this->db->where('id_pegawai', $id_pegawai);
        return $this->db->update('tb_pegawai', $param);
	}
	
	public function updateLogin ($id_user)
	{	
		
		$param = array(
            'sts_online' => '1',
        );
        $this->db->where('id_user', $id_user);
        return $this->db->update('tb_user', $param);
	}
	
	public function updateLogout ($id_user)
	{	
		date_default_timezone_set('Asia/Jakarta');
		$time = new dateTime();
		$time = $time->format('Y-m-d H:i:s');
		$param = array(
            'sts_online' => '0',
			'time' => $time,
        );
        $this->db->where('id_user', $id_user);
        return $this->db->update('tb_user', $param);
	}
}

?>